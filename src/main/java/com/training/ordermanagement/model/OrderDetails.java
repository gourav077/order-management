package com.training.ordermanagement.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="order_details")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "orders")
public class OrderDetails {


    @EmbeddedId
    @JsonIgnore
    private OrderDetailsPK pk;

    @Column(nullable = false) private Integer quantity;

    public OrderDetails() {
        super();
    }

    public OrderDetails(Orders order, Product product, Integer quantity) {
        pk = new OrderDetailsPK();
        pk.setOrder(order);
        pk.setProduct(product);
        this.quantity = quantity;
    }
    @Transient
    public Product getProduct() {
        return this.pk.getProduct();
    }

    @Transient
    public Double getTotalPrice() {
        return getProduct().getUnitPrice() * getQuantity();
    }

    public OrderDetailsPK getPk() {
        return pk;
    }

    public void setPk(OrderDetailsPK pk) {
        this.pk = pk;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((pk == null) ? 0 : pk.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderDetails other = (OrderDetails) obj;
        if (pk == null) {
            if (other.pk != null) {
                return false;
            }
        } else if (!pk.equals(other.pk)) {
            return false;
        }

        return true;
    }



}
