package com.training.ordermanagement.service;

import com.training.ordermanagement.model.OrderDetails;
import com.training.ordermanagement.repository.OrderDetailsRepo;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailsServiceImpl implements OrderDetailsService {
    private OrderDetailsRepo orderDetailsRepository;

    public OrderDetailsServiceImpl(OrderDetailsRepo orderDetailsRepository) {
        this.orderDetailsRepository = orderDetailsRepository;
    }

    @Override
    public OrderDetails create(OrderDetails orderDetails) {
        return this.orderDetailsRepository.save(orderDetails);
    }
}
