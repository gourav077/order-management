package com.training.ordermanagement.service;

import com.training.ordermanagement.exception.ResourceNotFoundException;
import com.training.ordermanagement.model.Customer;
import com.training.ordermanagement.repository.CustomerRepo;
import com.training.ordermanagement.repository.OrderDetailsRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepo customerRepository;

    public CustomerServiceImpl(CustomerRepo customerRepository) {
        this.customerRepository = customerRepository;
    }


   // public CustomerServiceImpl() {
    //    super();
    //}

    @Override
    public Customer getCustomerById(Long id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Customer not found"));
    }

    @Override
    public Customer addCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> getCustomer() {
        return customerRepository.findAll();
    }
}
