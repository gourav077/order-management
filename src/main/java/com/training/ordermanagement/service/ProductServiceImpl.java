package com.training.ordermanagement.service;

import com.training.ordermanagement.exception.ResourceNotFoundException;
import com.training.ordermanagement.model.Product;
import com.training.ordermanagement.repository.ProductRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService{
    private ProductRepo productRepository;

    public ProductServiceImpl(ProductRepo productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProduct(long id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product not found"));
    }
    @Override
  public   Product addProduct(Product product){
        return productRepository.save(product);
    }

    @Override
  public   Product updateProduct(Product product){



   //     product.setId(id);

        Product updatedProduct =productRepository.save(product);

     // = productRepository.save(product);
        return updatedProduct;
    }


}
