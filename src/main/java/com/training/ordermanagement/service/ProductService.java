package com.training.ordermanagement.service;

import com.training.ordermanagement.model.Product;

import javax.validation.constraints.Min;
import java.util.List;

public interface ProductService {
   List<Product> getAllProducts();

    Product getProduct(@Min(value = 1L, message = "Invalid product ID.") long id);
    Product addProduct(Product product);
    Product updateProduct(Product product);
}
