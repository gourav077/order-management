package com.training.ordermanagement.service;

import com.training.ordermanagement.model.Orders;
import org.aspectj.weaver.ast.Or;

import java.util.List;

public interface OrderService {
    Iterable<Orders> getAllOrders();
    Orders create(Orders order);
    void update(Orders order);
}
