package com.training.ordermanagement.service;

import com.training.ordermanagement.exception.ResourceNotFoundException;
import com.training.ordermanagement.model.Category;
import com.training.ordermanagement.repository.CategoryRepo;
import com.training.ordermanagement.repository.CustomerRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{
    private CategoryRepo categoryRepository;

    public CategoryServiceImpl(CategoryRepo categoryRepository) {
        this.categoryRepository = categoryRepository;
    }
    @Override
    public List<Category> getCategory() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategoryById(Long id) {
        return categoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Category not found"));
    }

    @Override
    public Category saveCategory(Category category) {
        return categoryRepository.save(category);
    }
}
