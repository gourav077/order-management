package com.training.ordermanagement.service;

import com.training.ordermanagement.model.Orders;
import com.training.ordermanagement.repository.OrderRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepo orderRepository;

    public OrderServiceImpl(OrderRepo orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Iterable<Orders> getAllOrders() {
        return this.orderRepository.findAll();
    }

    @Override
    public Orders create(Orders order) {
        //order.setDateCreated(LocalDate.now());

        return this.orderRepository.save(order);
    }

    @Override
    public void update(Orders order) {
        this.orderRepository.save(order);
    }
}
