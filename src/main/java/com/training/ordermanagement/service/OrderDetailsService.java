package com.training.ordermanagement.service;

import com.training.ordermanagement.model.OrderDetails;

public interface OrderDetailsService {
    OrderDetails create(OrderDetails orderDetails);
}
