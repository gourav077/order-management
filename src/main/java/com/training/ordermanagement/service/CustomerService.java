package com.training.ordermanagement.service;

import com.training.ordermanagement.model.Customer;

import java.util.List;

public interface CustomerService {
   public Customer getCustomerById(Long id);
   public Customer addCustomer(Customer customer);
   public List<Customer> getCustomer();
}
