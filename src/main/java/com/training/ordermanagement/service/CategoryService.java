package com.training.ordermanagement.service;

import com.training.ordermanagement.model.Category;

import java.util.List;

public interface CategoryService {
    public List<Category> getCategory();
    public Category getCategoryById(Long id);
    public Category saveCategory(Category category);
}
