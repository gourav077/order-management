package com.training.ordermanagement.controller;

import com.training.ordermanagement.dto.ProductDto;
import com.training.ordermanagement.exception.ResourceNotFoundException;
import com.training.ordermanagement.model.Customer;
import com.training.ordermanagement.model.OrderDetails;
import com.training.ordermanagement.model.Product;
import com.training.ordermanagement.repository.ProductRepo;
import com.training.ordermanagement.service.CategoryService;
import com.training.ordermanagement.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class ProductController {

    private ProductService productService;
    private CategoryService categoryService;

    public ProductController(ProductService productService,CategoryService categoryService) {
        this.productService = productService;
         this.categoryService=categoryService;
    }

    @GetMapping("/products")
    public List<Product> productList()
    {
        return productService.getAllProducts();
    }

    @PostMapping("/products/add")
    public Product addProduct(@RequestBody ProductDto pdto ){
        Product product=new Product();
       // productService.addProduct(product);
        product.setProductName(pdto.getProductName());
        product.setUnitPrice(pdto.getUnitPrice());
        product.setCategory(categoryService.getCategoryById(pdto.getCid()));
        product.setQuantityAvailable(pdto.getQuantityAvailable());
          this.productService.addProduct(product);
        return product;
    }

    @PutMapping("/products/update/{id}")
    public Product updateProduct(@RequestBody ProductDto pdto, @PathVariable(value = "id") Long id)
    {
        Product product=new Product();
        // productService.addProduct(product);
        product.setProductName(pdto.getProductName());
        product.setUnitPrice(pdto.getUnitPrice());
        product.setCategory(categoryService.getCategoryById(pdto.getCid()));
        product.setId(id);
        product.setQuantityAvailable(pdto.getQuantityAvailable());
        return    productService.updateProduct(product);
    }

    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable(value="id") Long productId)
    {
        return productService.getProduct(productId);
    }
}
