package com.training.ordermanagement.controller;

import com.training.ordermanagement.exception.ResourceNotFoundException;
import com.training.ordermanagement.model.Customer;
import com.training.ordermanagement.model.OrderDetails;
import com.training.ordermanagement.repository.OrderDetailsRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class OrderDetailsController {
    @Autowired
    OrderDetailsRepo orderDetailsRepo;


    @GetMapping("/orderDetails")
    public List<OrderDetails> orderDetailsList()
    {
        return orderDetailsRepo.findAll();
    }
    @PostMapping("/orderDetails/add")
    public OrderDetails createOrderDetails(@Valid @RequestBody OrderDetails orderDetails){
        return orderDetailsRepo.save(orderDetails);
    }
    @GetMapping("/orderDetails/{id}")
    public OrderDetails getOrderDetailsById(@PathVariable(value="id") Long orderDetailsId)
    {
        return orderDetailsRepo.findById(orderDetailsId).orElseThrow(() -> new ResourceNotFoundException("OrderDetails"));
    }


}
