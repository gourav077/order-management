package com.training.ordermanagement.controller;

import com.training.ordermanagement.dto.OrderProductDto;
import com.training.ordermanagement.exception.ResourceNotFoundException;
import com.training.ordermanagement.model.Customer;
import com.training.ordermanagement.model.OrderDetails;
import com.training.ordermanagement.model.Orders;
import com.training.ordermanagement.repository.OrderRepo;
import com.training.ordermanagement.service.CustomerService;
import com.training.ordermanagement.service.OrderDetailsService;
import com.training.ordermanagement.service.OrderService;
import com.training.ordermanagement.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/orders")
public class OrderController {

    ProductService productService;
    OrderService orderService;
    OrderDetailsService orderDetailsService;
    CustomerService customerService;
    public OrderController(ProductService productService, OrderService orderService, OrderDetailsService orderDetailsService,CustomerService customerService) {
        this.productService = productService;
        this.orderService = orderService;
        this.orderDetailsService = orderDetailsService;
        this.customerService=customerService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Orders> list() {
        return this.orderService.getAllOrders();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Orders create(@RequestBody OrderForm form) {
        List<OrderProductDto> formDtos = form.getProductOrders();
        validateProductsExistence(formDtos);
        Orders order = new Orders();
       // System.out.println(order.getOrderId());

        order = this.orderService.create(order);
        double sum=0;
        List<OrderDetails> orderDetails = new ArrayList<>();
        for (OrderProductDto dto : formDtos) {

            orderDetails.add(orderDetailsService.create(new OrderDetails(order, productService.getProduct(dto
                    .getProductId()), dto.getQuantity())));
            sum+=dto.getQuantity()*productService.getProduct(dto.getProductId()).getUnitPrice();
        }
        order.setTotalAmount(sum);
        order.setOrderDetails(orderDetails);
        order.setCustomer(customerService.getCustomerById(form.getCustomerId()));
        this.orderService.update(order);
        return  order;
    }
    private void validateProductsExistence(List<OrderProductDto> orderProducts) {
        List<OrderProductDto> list = orderProducts
                .stream()
                .filter(op -> Objects.isNull(productService.getProduct(op
                        .getProductId())))
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(list)) {
            new ResourceNotFoundException("Product not found");
        }
    }

    public static class OrderForm {

        private List<OrderProductDto> productOrders;

        public Long getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Long customerId) {
            this.customerId = customerId;
        }

        private Long customerId;

        public List<OrderProductDto> getProductOrders() {
            return productOrders;
        }

        public void setProductOrders(List<OrderProductDto> productOrders) {
            this.productOrders = productOrders;
        }
    }
}
