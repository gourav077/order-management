package com.training.ordermanagement.controller;

import com.training.ordermanagement.model.Category;
import com.training.ordermanagement.model.Customer;
import com.training.ordermanagement.repository.CustomerRepo;
import com.training.ordermanagement.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/customers")
    public List<Customer> getCustomers()
    {
        return customerService.getCustomer() ;
    }

    @PostMapping("/customer")
  public Customer saveCustomer(@RequestBody Customer customer)
    {
        return customerService.addCustomer(customer);
    }

    @GetMapping("/customer/{id}")
    public Customer getCustomerById(@PathVariable(value = "id") Long id)
    {
        return customerService.getCustomerById(id);
    }

}
