package com.training.ordermanagement.controller;

import com.training.ordermanagement.model.Category;
import com.training.ordermanagement.model.Customer;
import com.training.ordermanagement.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    List<Category> getCategory()
    {
       return categoryService.getCategory();
    }

    @PostMapping("/category")
    public Category addCategory(@RequestBody Category category)
    {
        return categoryService.saveCategory(category);
    }

    @GetMapping("/category/{id}")
    public Category getCategoryById(@PathVariable(value = "id") Long id)
    {
        return categoryService.getCategoryById(id);
    }

}
