package com.training.ordermanagement.dto;

import com.training.ordermanagement.model.Product;
import lombok.Getter;
import lombok.Setter;

public class OrderProductDto {



    private Integer productId;
    private Integer quantity;
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }


}
