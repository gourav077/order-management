package com.training.ordermanagement;

import com.training.ordermanagement.controller.OrderController;
import com.training.ordermanagement.controller.ProductController;
import com.training.ordermanagement.dto.OrderProductDto;
import com.training.ordermanagement.model.Orders;
import com.training.ordermanagement.model.Product;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.CoreMatchers.is;

class OrderManagementApplicationTests {

}
